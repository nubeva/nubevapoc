# nubevapoc
Nubeva Proof of Concept Files.  There are two different scripts in this repository.  One for Azure and one for AWS.
Both have very similar usages.

## Usage
Uses whatever account you are logged into currently with. Use  'az login' for Azure and 'aws configure' for AWS.

### Create Example 

Azure:
`./nubeva-poc-install-azure.sh -n nubevapoc -r westus -p NubevaCustomPass!`

AWS:
`./nubeva-poc-install-aws.sh -n nubevapoc -r us-west-2`
No passwords for AWS, the ssh key is automatically retreived from ~/.ssh/id_rsa.pub, but can be specified via -k or --public-key-file to create a keypair, or via -kn or --key-name to specify an existing keypair name inside AWS.

### Delete Example 
`./nubeva-poc-install -n nubevapoc -d`

### Arguments
```
-n|--name <name>
    REQUIRED
    The name of the POC resource group to create/delete
-r|--region <region>
    CONDITIONAL (Required for create only)
    The region to use for the POC resource group
-d|--delete
    Flag to schedule a delete of a POC environment, if not specified goes to
    create by default
-o|--offer <preview|live>
    Choose whether to use preview version of the Controller or live.
-k|--public-key-file <filename>
    Filename of the public key material to use to create an AWS keypair.  defaults for ~/.ssh/id_rsa.pub
    OPTIONAL
-kn|--key-name <keyname>
    Name of a keypair already existing in AWS
    OPTIONAL
-h|--help
    Display this help message
```
