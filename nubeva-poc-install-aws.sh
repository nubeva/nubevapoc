#!/bin/bash

set -e

# trap exit commands to force them to run cleanup first
trap cleanup_artefacts SIGINT


#set arguement equal to resource group
NAME=
REGION=
PROFILE=
PROFILE_TEXT=
OFFER=live
PUBLIC_KEY_FILE="$HOME/.ssh/id_rsa.pub"
DELETE=false
VERSION=false

TEMPLATE_URL=https://bitbucket.org/nubeva/nubevapoc/raw/master/awstemplate.yaml
TEMPLATE=awstemplate.yaml

check_region () {
    if [ -z "$REGION" ]
    then
        echo "Required argument region is unset, please specify using '-r <region>'"
        exit 1
    fi
}

# Display the help message for the script
help () {
    echo ""
    echo "Nubeva Proof-of-Concept (POC) enviroment launching script"
    echo "---------"
    echo "| USAGE |"
    echo "---------"
    echo "CREATE: ./nubeva-poc-install -n nubevapoc -r westus -p NubevaCustomPass!"
    echo "DELETE: ./nubeva-poc-install -n nubevapoc -r westus -d"
    echo ""
    echo "-------------"
    echo "| ARGUMENTS |"
    echo "-------------"
    echo "-n|--name <name>"
    echo "    REQUIRED"
    echo "    The name of the POC resource group to create/delete"
    echo "-r|--region <region>"
    echo "    CONDITIONAL (Required for create only)"
    echo "    The region to use for the POC resource group"
    echo "-o|--offer <preview|live>"
    echo "    Indicates whether to use the latest preview controller version"
    echo "    or the live marketplace offer. Preview requires whitelisting.  Defaults to $OFFER."
    echo "-d|--delete"
    echo "    Flag to schedule a delete of a POC environment, if not specified goes to"
    echo "    create by default"
    echo "-k|--public-key"
    echo "    Public key to use"
    echo "-kn|--key-name"
    echo "    The name of a pre-existing (in AWS) keypair to use when launching instances. If not provided"
    echo "    a keypair will be generated for you matching the provided --name using your id_rsa"
    echo "-p|--profile"
    echo "    The AWS profile to use when launching"
    echo "-tf|--template-file"
    echo "    Path to Nubeva's AWS template file. You may download from ${TEMPLATE_URL}"
    echo "-h|--help"
    echo "    Display this help message"
    echo ""

    #  Undocumented options:
    #  --version
    #      Uses the nubeva development, "master" version.
    #      Requires whitelisting to launch.
}

# Delete a resource group with a given name, to run pass in a -d|--delete flag
delete () {
    STACKID=$(aws cloudformation describe-stacks --region $REGION --stack-name $NAME --query "Stacks[0].StackId" --output text $PROFILE_TEXT)
    SS=$(get_stack_state)
    if [ -z "$SS" ]
    then
        echo "Unable to retrieve stack $NAME status.  Does it exist?"
        exit 1
    fi
    if echo "$SS" | grep -q DELETE
    then
        echo "Currently deleting"
        return
    fi
    read -p "Are you sure you would like to delete stack '$NAME'? [y/n]: " -r
    if [[ $REPLY =~ ^[Yy]$ ]]
    then
        echo ""
        echo "Deleting stack"
        echo ""
        aws cloudformation --region $REGION delete-stack --stack-name $NAME $PROFILE_TEXT
    fi
}

# Create a resource group with a give name in a given region (-n|--name, -r|--region)
create () {
    echo "Setting offer parameters"
    #create resource group
    OFFERBASE="controller-2.*"
    if [ -n "$VERSION" ]
    then
        OFFERBASE="controller-$VERSION"
    fi
    # TODO: need a better way to filter.  Currently assuming the major version is 2 and the most recently created is the highest version.  Neither are always true
    CONTROLLER_AMI_ID=$(aws ec2 describe-images --region $REGION --filters Name=name,Values=$OFFERBASE --query 'Images | reverse(sort_by(@, &CreationDate))[0].ImageId' --output text $PROFILE_TEXT)
    if [ "$CONTROLLER_AMI_ID" == "None" ]
    then
        CONTROLLER_AMI_ID=$(aws ec2 describe-images --region $REGION --filters Name=name,Values=controller-1.* --query 'Images | reverse(sort_by(@, &CreationDate))[0].ImageId' --output text $PROFILE_TEXT)
    fi
    echo "Using Controller AMI ID ${CONTROLLER_AMI_ID}"

    if [ "$CONTROLLER_AMI_ID" == "None" ]
    then
        echo "Unable to find the controller AMI ID for offer $OFFERBASE"
        exit 1
    fi
    echo "Using Controller AMI ID ${CONTROLLER_AMI_ID}"

    AMZN_AMI_ID=$(aws ec2 describe-images --region $REGION --owners=137112412989 --filters Name=virtualization-type,Values=hvm,Name=name,Values=amzn-ami-hvm-*,Name=root-device-type,Values=ebs --query 'Images | reverse(sort_by(@, &CreationDate))[0].ImageId' --output text $PROFILE_TEXT)
    echo "Using Amzn AMI ID ${AMZN_AMI_ID}"

    # deploy aws template
    echo Deploying AWS Template

    if [ -f "$TEMPLATE" ]
    then
        echo "from local file"
        TEMPLATE_PARAM="--template-body file://$TEMPLATE"
    else
        echo "cannot find file ${TEMPLATE}. Please use -tf|--template-file to specify downloaded aws template path."
        exit 1
    fi
    STACKID=$(aws cloudformation create-stack --capabilities CAPABILITY_IAM CAPABILITY_NAMED_IAM  $TEMPLATE_PARAM --region $REGION --stack-name $NAME --parameters ParameterKey=KeyName,ParameterValue=$KEY_NAME ParameterKey=LinuxAmiId,ParameterValue=$AMZN_AMI_ID ParameterKey=ControllerAmiId,ParameterValue=$CONTROLLER_AMI_ID --query "StackId" --output text $PROFILE_TEXT)
    echo "Made stack id $STACKID"
}

get_stack_state() {
    if [ ! -z "$STACKID" ]
    then
        SID="$STACKID"
    else
        SID="$NAME"
    fi
    aws cloudformation --region $REGION describe-stacks --stack-name $SID --query 'Stacks[0].StackStatus' --output text $PROFILE_TEXT
}

wait_for_stack() {
    CURRENT_COUNT=$(aws cloudformation --region $REGION describe-stack-resources --stack-name $STACKID --query 'length(StackResources)' $PROFILE_TEXT)
    if [ "$1" == "delete" ]
    then
        echo -en " Waiting on stack $NAME deletion\r"
        RESOURCE_TARGET=0
        DENOMINATOR=$CURRENT_COUNT
    else
        echo -en " Waiting on stack $NAME creation\r"
        RESOURCE_TARGET=$(aws cloudformation get-template-summary --region $REGION --stack-name $STACKID --query 'length(ResourceTypes)' $PROFILE_TEXT)
        DENOMINATOR=$RESOURCE_TARGET
    fi

    while [ $CURRENT_COUNT -ne $RESOURCE_TARGET ]
    do
        CURRENT_COUNT=$(aws cloudformation --region $REGION describe-stack-resources --stack-name $STACKID --query "length(StackResources[?ResourceStatus != 'DELETE_COMPLETE'])" $PROFILE_TEXT)
        SS=$(aws cloudformation --region $REGION describe-stacks --stack-name $STACKID --query 'Stacks[0].StackStatus' --output text $PROFILE_TEXT)
        if [ $DENOMINATOR -ne 0 ]
        then
            PERCENT=$(( $CURRENT_COUNT * 100 / $DENOMINATOR))
            if [ "$1" == "delete" ]
            then
                PERCENT=$(( 100 - $PERCENT ))
            fi
        else
            PERCENT='??'
        fi
        echo -ne " Waiting on stack $NAME ($PERCENT%)  $SS               \r"
        if echo "$SS" | grep -q COMPLETE
        then
            if [ $PERCENT -ne 100 ]
            then
                echo
                echo "----8<----8<----8<----"
                echo "       DEBUG          "
                echo "----8<----8<----8<----"
                if [ "$!" == "delete" ]
                then
                    aws cloudformation describe-stack-events --region $REGION --stack-name $STACKID --query "StackEvents[?ResourceStatus == 'DELETE_FAILED'].{ResourceStatus:ResourceStatus,ResourceStatusReason:ResourceStatusReason,ResourceType:ResourceType,LogicalResourceId:LogicalResourceId}" $PROFILE_TEXT
                else
                    aws cloudformation describe-stack-events --region $REGION --stack-name $STACKID --query "StackEvents[?ResourceStatus == 'CREATE_FAILED'].{ResourceStatus:ResourceStatus,ResourceStatusReason:ResourceStatusReason,ResourceType:ResourceType,LogicalResourceId:LogicalResourceId}" $PROFILE_TEXT
                fi
                echo "----8<----8<----8<----"
                echo
                echo "ERROR: Stack Completed before resources match expectation of $RESOURCE_TARGET."
                exit 1
            fi
       else
           sleep 10
       fi
    done
    #  while not complete:
    while ! get_stack_state | grep -q COMPLETE
    do
        echo
        echo "Waiting on stack Completion."
        sleep 10
    done
    echo
    if [ "$1" == "delete" ]
    then
        echo "Stack DELETED"
    else
        echo "Stack COMPLETED."
    fi
}

function show_outputs() {
    aws cloudformation describe-stacks --region $REGION --stack-name $NAME --query "Stacks[0].Outputs" --output table $PROFILE_TEXT
}


# Argparsing
#######
# Make sure they match the help in the root

while [[ $# -gt 0 ]]
do
    key="$1"
    case $key in
        --version)
            VERSION=$2
            OFFER="preview"
            shift
            shift
            ;;
        -d|--delete)
            DELETE=true
            shift
            ;;
        -h|--help)
            help
            exit 0
            ;;
        -n|--name)
            NAME=$2
            shift
            shift
            ;;
        -r|--region)
            REGION=$2
            shift
            shift
            ;;
        -o|--offer)
            OFFER=$2
            shift
            shift
            ;;
        -k|--public-key-file)
            PUBLIC_KEY_FILE=$2
            shift
            shift
            ;;
        -kn|--key-name)
            KEY_NAME=$2
            shift
            shift
            ;;
        -p|--profile)
            PROFILE=$2
            PROFILE_TEXT="--profile $PROFILE"
            shift
            shift
            ;;
        -tf|--template-file)
            TEMPLATE=$2
            shift
            shift
            ;;
        *)
            echo "Unknown argument '$key', skipping..."
            shift
            ;;
    esac
done

#echo "DELETE   = $DELETE"
#echo "REGION   = $REGION"
#echo "NAME     = $NAME"

if [ -z "$NAME" ]
then
    echo "Required argument resource group name is unset, please specify using '-n <name>'"
    exit 1
fi

if [ -z "$KEY_NAME" ] && ! $DELETE
then
    check_region
    echo "Checking for key pair named $NAME"
    if ! aws ec2 --region $REGION describe-key-pairs --key-names $NAME $PROFILE_TEXT > /dev/null 2>&1 
    then
        if [ ! -e "$PUBLIC_KEY_FILE" ]
        then
            echo "Unable to find public key_file $PUBLIC_KEY_FILE.  Cannot create key.  Specify either --key-name or --public-key-file"
            exit 1
        fi
        echo "Creating keypair from $PUBLIC_KEY_FILE"
        aws ec2 --region $REGION import-key-pair --key-name $NAME --public-key-material "$(cat $PUBLIC_KEY_FILE)" $PROFILE_TEXT
        if [ $? == 0 ]
        then
            echo "Made key-pair named $NAME in region $REGION"
        else
            echo "Unable to make the key-pair named $NAME.  Specify the keypair name with --key-name"
            exit 1
        fi
    fi
    KEY_NAME="$NAME"
fi

if [ "$DELETE" = true ]
then
    delete
    wait_for_stack delete
else
    check_region
    create
    wait_for_stack
    show_outputs
fi

