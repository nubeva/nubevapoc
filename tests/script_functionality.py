from testutil.mys import MyS
import time
import sys
import os

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

NUBEVAPOC_AWS_SCRIPT_PATH = "../nubeva-poc-install-aws.sh"
RG_NAME = "NubPOC{}".format(int(time.time()))[:14]
AWS_REGION = "us-west-2"


def aws_script_launch(version=None):
    cmd = "./{} -n AWS{} -r {} -kn nubeva-ce -tf {}".format(NUBEVAPOC_AWS_SCRIPT_PATH, RG_NAME, AWS_REGION, "../awstemplate.yaml")
    if version:
        cmd += " --version {}".format(version)

    mys_client = MyS()
    out, err = mys_client.run_cmd(cmd, shell=True)
    print("\nOUTPUT:\n{}\n\nERROR:\n{}".format(out, err))
    return out, err


def aws_script_delete():
    cmd = "echo y | ./{} -n AWS{} -r {} -kn nubeva-ce --delete".format(NUBEVAPOC_AWS_SCRIPT_PATH, RG_NAME, AWS_REGION)
    mys_client = MyS()
    out, err = mys_client.run_cmd(cmd, shell=True)
    print("\nOUTPUT:\n{}\n\nERROR:\n{}".format(out, err))
    return out, err


class TestAwsLaunchAndSetup:
    @staticmethod
    def test_aws_script_launch_delete():
        out, err = aws_script_launch()
        assert out is not None
        out, err = aws_script_delete()
        assert out is not None
