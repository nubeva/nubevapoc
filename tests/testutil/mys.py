import os
import re
import json
import subprocess
import threading
import pprint
import errno


class MyS(object):
    """
    Nubeva Base class for various utility methods
    """
    # Debug

    nubeva_error_header = "*** NUBEVA ERROR MESSAGE ***"

    def __init__(self):
        self.sprocs = {}

    def run_cmd(self, cmd, params=None, verbose=False, shell=False, dump_first=False, error_out=False, timeout=None,
                pid=None, background=False):
        """ wrapper around the mys
        :param cmd:
        :param params: (list) list of parameters
        :param verbose:
        :param shell: boolean - subprocess executed as a shell
        :param dump_first: bool - iff True: dump the output before loading it: useful if _mys returns a python object
        :param error_out: bool - if you want to raise a ValueError if return code running command is not 0
        :param timeout: int - Timeout process (in seconds)
        :param pid:
        :param background: bool
        :return:
        """
        if isinstance(cmd, str):
            cmd = cmd.split()
            if params:
                cmd += params

        if shell:
            cmd = ' '.join(cmd)

        try:
            if background:
                mys_pid = self._mys(cmd, pid=pid, background=background, verbose=verbose, timeout=timeout)
                self._out("Asked to run in background. Returning pid {} for cmd: {}".format(mys_pid, cmd))
                return mys_pid
            output, error = self._mys(cmd, pid=pid, verbose=verbose, timeout=timeout)
        except TypeError as badtype:
            if badtype.message == "execv() arg 2 must contain only strings":
                print("cmd is bad type {}: {}".format(type(cmd), cmd))
            raise badtype
        output = str(output).strip().rstrip()

        if verbose and output:
            print("\nOutput:")
            print(output)

        if verbose and error:
            print("\nError:")
            print(error)

        if error_out:
            if re.search("Got Error \d+ calling command", error) or re.search("Process returned error \d+", error):
                raise ValueError(error)

        if dump_first and output:
            output = json.dumps(output)

        try:
            output = json.loads(output) if output else None
            if verbose:
                pprint.pprint(output, indent=4)
            return output, error
        except ValueError as v_err:
            if "No JSON object could be decoded" in v_err.message or "Extra data" in v_err.message:
                # if we cannot parse the output into a json, return it as the string.
                return output if output else None, error
            print("Invalid  output: ", output)
            raise

    def _mys(self, cmd, env=None, log_errors=False, dry_run=False, out=subprocess.PIPE, err=subprocess.PIPE,
             verbose=True, timeout=None, background=False, pid=None):
        """
            :param cmd: (list/str) Command can be either. shell=True is used when cmd is a str
            :param env:
            :param log_errors: (bool)
            :param dry_run: (bool) Print command, but do not execute
            :param out:
            :param err:
            :param verbose:
            :param timeout: (int) Timeout process in seconds
            :return:
            """
        # If cmd is a lists, then call it as arguments without a shell
        # Otherwise, assume it is a string and needs shell=True
        if verbose:
            print("Calling command: {}".format(cmd))

        if pid:
            proc = self.sprocs[pid]['proc']
        else:
            shell = True
            if isinstance(cmd, list):
                shell = False

            # Update environment if need be
            r = os.environ.copy()
            if env:
                r.update(env)

            # If dry run, print command and exit method
            if dry_run:
                msg = "Running: {}".format([cmd, "shell={}".format(shell)])
                if env:
                    msg = "Running: {}".format([cmd, "env={}".format(r), "shell=".format(shell)])
                print(msg)
                return

            # Execute command using subprocess
            if env:
                proc = subprocess.Popen(cmd, env=r, shell=shell, stdout=out, stderr=err)
            else:
                proc = subprocess.Popen(cmd, shell=shell, stdout=out, stderr=err)

        if background:
            pid = proc.pid
            self.sprocs[pid] = {'proc': proc, 'cmd': cmd}
            return pid

        # Timeout and kill process
        if timeout:
            timer = threading.Timer(timeout, proc.kill)
            try:
                timer.start()
                (output, error) = proc.communicate()
            finally:
                timer.cancel()
        else:
            (output, error) = proc.communicate()

        if pid:
            del self.sprocs[pid]

        nubeva_error_message = ""
        if log_errors and error and verbose:
            nubeva_error_message += "{}\nProcess returned error {}".format(self.nubeva_error_header, error)
        if proc.returncode < 0:
            nubeva_error_message += "{}\nGot negative return code, hit resource limit on subprocess or " \
                                    "process killed".format(self.nubeva_error_header)
            nubeva_error_message += "\nProcess returned error {}".format(error)
            nubeva_error_message += "\nGot negative return code, hit resource limit on subprocess or process killed"
        if proc.returncode != 0:
            nubeva_error_message += "{}\nProcess return output {}".format(self.nubeva_error_header, output)
            nubeva_error_message += "\nProcess returned error {}".format(error)
            nubeva_error_message += "\nGot Error {} calling command '{}'".format(proc.returncode, cmd)

        if nubeva_error_message:
            error = "{}\n\n{}".format(error, nubeva_error_message)

        # reap zombies
        if not self.sprocs:
            try:
                os.waitpid(-1, os.WNOHANG)
            except:
                pass

        return output, error

    @staticmethod
    def _out(msg):
        print("\n[MyS] {}".format(msg))

    @staticmethod
    def is_running(pid):
        try:
            os.kill(pid, 0)
        except OSError as err:
            if err.errno == errno.ESRCH:
                return False
        return True
